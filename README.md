# ZGLoot
Modified addon ZgLoot. Now you can automate looting of scarabs and idols in AQ 20 and AQ 40 too. Also have some nice options, like work with roll messages, and looting items from trash.

## Installation
1. Download **[Latest Version](https://gitlab.com/Artur91425/ZGLoot/-/archive/master/ZGLoot-master.zip)**
2. Unpack the Zip file
3. Rename the folder "ZGLoot-master" to "ZGLoot"
4. Copy "ZGLoot" into Wow-Directory\Interface\AddOns\
5. Restart WoW
